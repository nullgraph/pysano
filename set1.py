#!/usr/bin/env python

from bitarray import bitarray

table64="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"


def main():
	s="deadbeef"
	h=hex2base64(s,table64)
	a=bitarray()
	a.frombytes(s)
	print a
	t="Derb7v"
	b=bitarray()
	b.frombytes(t)
	print b



def hamming_distance(s1,s2):
	"""return the Hamming distance of two equal length hex-encoded strings"""
	e1=bitarray(); e2=bitarray()
	e1.frombytes(s1)
	e2.frombytes(s2)
	return (e1^e2).count()

def repeatXOR(plaintext,key):
	"""Input: plaintext in English, key in English, output hex-encoded xor repeating-key"""
	keylen=len(key)
	cipher=[]
	for i in xrange(len(plaintext)):
		cipher.append("{:0>2x}".format(ord(key[i%keylen])^ord(plaintext[i])))
	return "".join(cipher)


def freq_count(en):
	"""count all occurences of all characters in the input English string"""
	table={}
	for c in en:
		if table.has_key(c):
			table[c]=table[c]+1
		else:
			table[c]=1
	return table

def grade(en):
	"""given an English sentence, determine how close it is to English"""
	allowedord=[32,33,34,40,41,46,63,]+range(48,60)+range(65,91)+range(97,123)
	entop="ETAOIN SHRDLU".lower()
	# entop="ETAOIN ".lower()
	table=freq_count(en)
	top="".join(sorted(table, key=table.get, reverse=True)[:len(entop)])
	score=0
	for c in top:
		if c in entop:
			score=score+table[c]
	return float(score)/float(len(table))

def brute_force_XOR(s):
	"""brute force the XOR cipher by trying all combinations"""
	ranking=[]
	for c in xrange(255): #test through all possible bytes
		en=singleXOR(s,chr(c))
		ranking.append([c,grade(en)])
	win=sorted(ranking,key=lambda item: item[1],reverse=True)[0]
	return [chr(win[0]),singleXOR(s,chr(win[0])),win[1]]


def singleXOR(s,c):
	"""XOR hex-encoded s against the repetition of an English character c,
	return an ascii string"""
	a=hex2ascii(s)
	b=[chr(ord(i)^ord(c)) for i in a]
	return "".join(b)

def fixedXOR(s1,s2):
	"""take two equal-length hex-encoded strings and produce their XOR"""
	a1=hex2ascii(s1)
	a2=hex2ascii(s2)
	a3=[chr(ord(a)^ord(b)) for a,b in zip(a1,a2)]
	return "".join(a3)

def hex2ascii(h):
	"""convert hex string to ascii string"""
	#test 6465616462656566=deadbeef
	s=[]
	for i in xrange(0,len(h),2):
		s.append(chr(int(h[i:i+2],16)))
		i=i+2
	return ''.join(s)

def ascii2hex(en):
	return "".join([format(ord(c),'x') for c in en])

def hex2base64(s,table64):
	"""take a hex string and convert it to a base64 string"""
	#6f6d=9t
	t=[]
	dec=int(s,16)
	q=dec>>6
	r=dec&63
	t.insert(0,table64[r])
	while q>=64:
		r=q&63
		q=q>>6
		t.insert(0,table64[r])
	t.insert(0,table64[q])
	return ''.join(t)


def test_hamming():
	#37
	s1="this is a test"
	s2="wokka wokka!!!"
	print hamming_distance(s1,s2)

def problem5():
	#0b3637272a2b2e63622c2e69692a23693a2a3c6324202d623d63343c2a26226324272765272
	#a282b2f20430a652e2c652a3124333a653e2b2027630c692b20283165286326302e27282f
	plaintext="Burning 'em, if you ain't quick and nimble\nI go crazy when I hear a cymbal"
	key="ICE"
	print repeatXOR(plaintext,key)

def problem4():
	lines = [line.strip() for line in open('4.txt')]
	decoded=[]
	for line in lines:
		decoded.append(brute_force_XOR(line))
	print sorted(decoded,key=lambda item: item[2],reverse=True)[0]

def problem3():
	s="1b37373331363f78151b7f2b783431333d78397828372d363c78373e783a393b3736"
	print brute_force_XOR(s)

def problem2():
	s1="1c0111001f010100061a024b53535009181c"
	s2="686974207468652062756c6c277320657965"
	print fixedXOR(s1,s2)

def problem1():
	s="49276d206b696c6c696e6720796f757220627261696e206c696b65206120706f69736f6e6f7573206d757368726f6f6d"
	print hex2base64(s,table64)

main()
